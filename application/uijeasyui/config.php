<?php

/*
 * 项目配置文件
 * @since version:1.0(2016-11-4); author:SoChishun(14507247@qq.com); desc:Added.
 */
return array(
    /* 模板输出替换 */
    'view_replace_str' => [
        '__PUBLIC__' => '/public/',
        '__ROOT__' => '/',
        '__THEME__'=>'/public/static/project/uijeasyui/',
        '__SCRIPTLIB__'=>'/public/static/scriptlib/',
        '__CSSLIB__'=>'/public/static/csslib/',
    ]
);
