<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\uijeasyui\controller;

/**
 * Extension 类
 *
 * @since version:1.0(2016-11-8); author:SoChishun(14507247@qq.com); desc:Added.
 */
class Extension extends Appbase {

    /**
     * 分机列表
     * @return type
     */
    function extension_list() {
        return view();
    }

    /**
     * 分机列表数据
     * @return type
     * @since version:1.0(2016-11-8);
     */
    function get_extension_list_json() {
        return json(["total"=>1,'rows'=>[["id"=>"1001","id_name"=>"Extension 1001","user_context"=>"default","login_time"=>"2016-11-8 14:59:30","create_time"=>"2016-11-8 13:00:23","online_status_text"=>"离线","status"=>"可用"]]]);
    }

}
